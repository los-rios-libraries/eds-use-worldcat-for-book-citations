# README #

## NO LONGER UPDATED ##
This has been integrated into the EDS-Complete repository and any further changes will be made there.

* * *

In EDS, catalog citations are very messed up. EBSCO is unable to separate out the author and title in the 245 field, plus it treats the record as if it's an online database record. So this script, placed in a detailed record widget, replaces the "cite" link function with one that finds the OCLC WorldCat citation page and puts it where EBSCO's normally would be.

It's not perfect. If you use it, then click one of the other tools, then click it again, it has some funky behavior. But it should work in most cases.

![screenshot](https://bitbucket.org/repo/GEr6zq/images/811865766-screenshot.png)

## To Do ##

* Look more at detailed record markup to fix the problems
* Also allow emailing of citations (might require screen scraping)