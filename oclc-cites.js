// hide citation link if LOIS record and replace with OCLC if OCLC no. is present
var isCatRec = window.parent.document.querySelector('.record-has-rtac');
var tpContent = window.parent.document.getElementById('ToolPanelContent');

function openCitePage(page) {
  var panelWrapper = window.parent.document.querySelector('#ToolPanelContent .wrapper');
  if (window.parent.document.getElementById('citeDialog') === null) {
    var closePanel = window.parent.document.querySelector('.close-panel');
    closePanel.style.display = 'block';
    var citePanel = window.parent.document.createElement('div'); // here we're imitating how EBSCO makes the other tool panels
    citePanel.id = 'citeDialog';
    citePanel.setAttribute('class', 'border');
    var citeTitle = window.parent.document.createElement('h2');
    citeTitle.setAttribute('class', 'panel-header color-p2');
    citeTitle.innerHTML = '<span class="icon"></span> Citation Format';
    var panelContent = window.parent.document.createElement('div');
    panelContent.setAttribute('class', 'panel-content');
    panelContent.innerHTML = '<p class="informational-note"><span class="icon"></span><em>NOTE:</em> The citations below are supplied by WorldCat.org. Please beware of missing data. In some cases cities, publishers and dates may be missing.</p>'; // might as well do it this way rather than create each individual element... might make things easier to do this with some of the othe relements, too. Could create citePanel and do innerHTML of that.
    var panelResults = window.parent.document.createElement('div');
    panelResults.setAttribute('class', 'panel-results');
    var citeFrame = window.parent.document.createElement('iframe');
    citeFrame.setAttribute('src', page);
    citeFrame.setAttribute('width', '100%');
    citeFrame.setAttribute('height', '360');
    tpContent.style.display = 'block';
    // put it all together
    citePanel.appendChild(citeTitle);
    citePanel.appendChild(panelContent);
    citePanel.appendChild(panelResults);
    panelResults.appendChild(citeFrame);
    panelWrapper.appendChild(citePanel);
    window.parent.document.querySelector('.close-panel').addEventListener('click', function() {
      closeCitePanel(panelWrapper, citePanel, closePanel);
    });
    var otherDialog = window.parent.document.getElementById('printEmailDialog'); // fix for display errors when using multiple tools
    if (otherDialog) {
      otherDialog.setAttribute('class', 'hidden');
    }
  }
}

function closeCitePanel(a, b, c) {  // getting an error still... Uncaught NotFoundError: Failed to execute 'removeChild' on 'Node': The node to be removed is not a child of this node. Only happens when I've also used a different tool. But the panel still closes, despite the error.
  a.removeChild(b);
  c.style.display = 'none';
  tpContent.style.display = 'none';
  c.removeEventListener('click', closeCitePanel);
}

function findWorldCatURL() {
  var oclc = 'ep.OCLC'; // these are special tokens supplied by EBSCO for widgets
  var isbn = 'ep.ISBN';
  var identifier;
  var citePage;
  // prefer oclc, but use isbn if it's available and oclc is not
  if (oclc !== '') {
    identifier = 'oclc/' + oclc.replace('ocm', ''); // replace bad characters in some of our oclc links
  } else if (isbn !== '') {
    identifier = 'isbn/' + isbn;
  } else {
    identifier = '';
  }
  if (identifier !== '') {  // if there's no isbn and no oclc number, then the shitty EBSCO citations are shown
    citePage = 'http://www.worldcat.org/' + identifier + '?page=citation';
  } else {
    citePage = '';
  }
  return citePage;
}
(function() {
  if (isCatRec) {
    var worldCatURL = findWorldCatURL();
    if (worldCatURL !== '') {
      var citeLink = window.parent.document.querySelector('a.cite-link');
      citeLink.removeAttribute('data-panel');
      citeLink.setAttribute('href', worldCatURL); // good practice to show user the URL on hover. Can open in new window if preferred
      citeLink.setAttribute('target', '_blank');
      citeLink.addEventListener('click', function(e) {
        e.preventDefault();
        openCitePage(worldCatURL);
      });
    }

  }
})();